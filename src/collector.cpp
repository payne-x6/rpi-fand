#include "collector.hpp"

#include <cstring>

extern "C" {
#include <interface/vmcs_host/vc_vchi_gencmd.h>
#include <interface/vmcs_host/vc_gencmd_defs.h>
}

#include "exception.hpp"

namespace rf {

std::tuple<timestamp_t,	double>
measure_temp()
{
	char buffer[GENCMDSERVICE_MSGFIFO_SIZE]; buffer[0] = '\0';
	double time_diff;
	int ret;

	auto before = std::chrono::high_resolution_clock::now();
	if ((ret = vc_gencmd(buffer, sizeof(buffer), "measure_temp")) != 0) {
	 	throw rf::Exception("Unable to contact vc.", ret);
	}
	auto after = std::chrono::high_resolution_clock::now();
	
	buffer[sizeof(buffer) - 1] = 0;
	if (buffer[0] != '\0') {
		if (strncmp(buffer, "error=", 6) == 0) {
			int _errno = 0;
			std::sscanf(buffer, "error=%d", &_errno);
			char *message_start = std::strchr(buffer, '"') + 1;
			if (message_start == nullptr) {
				throw rf::Exception("Unable to read response from gencmd", _errno);
			}
			char *message_end = std::strchr(message_start, '"');
			*message_end = '\0';
			throw rf::Exception(message_start, _errno);
		} else {
			auto mean_time = before + (after - before) / 2.0;
			double temp = 0.0;
			std::sscanf(buffer, "temp=%lf'C", &temp);
			return std::make_tuple(mean_time, temp);
		}
	}
	throw rf::Exception("Unable to read response from gencmd.");
}

}