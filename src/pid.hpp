#pragma once

class PID final
{
public:
	PID(const double Kp, const double Kd = 0.0, const double Ki = 0.0, const double sp = 1.0);
	~PID() {};
	const double evaluate(double ts, double pv);
private:
        const double _Kp;
        const double _Kd;
        const double _Ki;
	const double _sp;
        double _last_ts;
        double _pre_error;
        double _integral;
};