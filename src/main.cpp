#include <algorithm>
#include <cerrno>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <chrono>
#include <fcntl.h>
#include <iomanip>
#include <iostream>
#include <tuple>

#include <sys/types.h>
#include <sys/stat.h>

#include <yaml-cpp/yaml.h>

extern "C" {
#include <interface/vmcs_host/vc_vchi_gencmd.h>
#include <interface/vmcs_host/vc_gencmd_defs.h>
}

#include "collector.hpp"
#include "configuration_parser.hpp"
#include "configuration.hpp"
#include "exception.hpp"
#include "pid.hpp"

static std::string config_path = "/etc/rpi-fan/config.yaml";
static std::string pid_storage = "/var/lib/rpi-fand/pid.map";
static std::string stats_socket = "/var/run/rpi-fand/stats.unix";

static void
show_usage()
{
	std::cout << "Daemon that measures temperature from Rasperry Pi's VideoCore and adjust speed of fan accordingly.\n" << std::endl;
	std::cout << "Usage:" << std::endl;
	std::cout << "  $ rpi-fand <-h|--help>" << std::endl;
	std::cout << "    Prints this message";
	std::cout << "  # rpi-fand [<-c|--config> config_file]" << std::endl;
	std::cout << "    Starts daemon" << std::endl;
	std::cout << "Options:" << std::endl;
	std::cout << "  -h, --help  Show this information\n" << std::endl;
	std::cout << "Exit status:" << std::endl;
	std::cout << "   0    Daemon exited successfully" << std::endl;
}

static constexpr unsigned long
ReLU(long input) {
	return std::max(input, 0L);
}

int
main(int argc, char **argv)
{
	auto prog_start = std::chrono::high_resolution_clock::now();
	int ret = EXIT_SUCCESS;
	if (argc > 1) {
		if (std::strcmp(argv[1], "-h") == 0 || std::strcmp(argv[1], "--help") == 0) {
			show_usage();
			return ret;
		} else if (std::strcmp(argv[1], "-c") == 0 || std::strcmp(argv[1], "--config") == 0) {
			config_path = argv[2];
		}
	}

	VCHI_INSTANCE_T vchi_instance;
	vcos_init();
	if (vchi_initialise(&vchi_instance) != 0) {
		std::cerr << strerror(EPERM) << std::endl;
		return EPERM;
	}
	if (vchi_connect( NULL, 0, vchi_instance ) != 0) {
		std::cerr << strerror(ECONNREFUSED) << std::endl;
		return ECONNREFUSED;
	}

	VCHI_CONNECTION_T *vchi_connection = nullptr;
	vc_vchi_gencmd_init(vchi_instance, &vchi_connection, 1);

	bool cont = true;
	while(cont) {
		cont = false;
		YAML::Node config = YAML::LoadFile(config_path);
		auto conf = config.as<rf::conf_parsed>();
		rf::Configuration::reload(config_path);

		mkfifo("/home/jan/Projects/rpi-fand/stats", 0444);


		PID pid(conf._global._pid._p, conf._global._pid._d, conf._global._pid._i, rf::conf().temperature);
		int fd = -1;
		for (int i = 0; i < 20; ++i) {
			try {
				auto [time, temperature] = rf::measure_temp();
				auto temp_time = std::chrono::duration<double>(time - prog_start);
				auto speed = ReLU(std::lround(pid.evaluate(temp_time.count(), temperature)));
				if (fd < 0) {
					fd = open("/home/jan/Projects/rpi-fand/stats", O_WRONLY | O_NONBLOCK);
					std::cout << "Opened stats: " << fd << std::endl;
				}
				if (fd >= 0) {
					FILE *f = fdopen(fd, "w");
					std::fprintf(f, "%f\t%f\t%lud\n", temp_time.count(), temperature, speed);
					if (ferror(f) || fflush(f)) {
						std::cout << "Closing stats: " << ret << std::endl;
						fclose(f);
						fd = -1;
					}
				}
				std::cout << std::fixed << std::setprecision(4)
				          << temp_time.count() << "\t" 
				          << std::setprecision(2) << temperature << "\t"
				          << speed
				          << std::endl;
			} catch (rf::Exception &e) {
				std::cerr << e.what() << std::endl;
				ret = ECONNRESET;
				break;
			}
			sleep(rf::conf().refresh_rate);
		}
	}

	vc_gencmd_stop();
	if (vchi_disconnect(vchi_instance) != 0) {
		std::cerr << strerror(ECONNRESET)<< std::endl;
		return ECONNRESET;
	}
	
	if (ret) {
		std::cerr << strerror(ret)<< std::endl;
	}
	return ret;
}