#include "configuration_parser.hpp"

namespace rf {

conf_time_interval::operator unsigned int() {
	return _interval;
}

conf_temperature::operator double() {
	return _temp;
}

}