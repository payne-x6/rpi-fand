#include "fan.hpp"

namespace rf {

Fan::Fan(const Conf_Fan &conf) :
	_offset(conf.offset),
	_threshold(conf.threshold)
{
	for (auto& policy : conf.policies) {
		if(Conf_Policy_ActiveThreshold *p = dynamic_cast<Conf_Policy_ActiveThreshold *>(policy.get())) {
			if(p->enabled) {
				_policies.push_back(std::make_unique<Policy_Active_Threshold>());
			}
		} else if(Conf_Policy_Timeout *p = dynamic_cast<Conf_Policy_Timeout *>(policy.get())) {
			if(p->timeout) {
				_policies.push_back(std::make_unique<Policy_Active_Threshold>());
			}
		}
	}
}

}