#pragma once

#include <string>
#include <memory>
#include <vector>

#include "configuration_parser.hpp"

namespace rf {

class Conf_Policy {
public:
	virtual ~Conf_Policy() {}
};

class Conf_Policy_ActiveThreshold final : public Conf_Policy {
public:
	const bool enabled;
	
	Conf_Policy_ActiveThreshold(const bool enabled);
	virtual ~Conf_Policy_ActiveThreshold() {}
};

class Conf_Policy_Timeout final : public Conf_Policy {
public:
	const unsigned int timeout;

	Conf_Policy_Timeout(const unsigned int timeout);
	virtual ~Conf_Policy_Timeout() {}
};

class Conf_Fan final {
public:
	const unsigned int offset;
	const unsigned int threshold;
	const std::vector<std::shared_ptr<Conf_Policy>> policies;

	Conf_Fan(const unsigned int offset, const unsigned int threshold, std::vector<std::shared_ptr<Conf_Policy>> &policies);
};

class Configuration final {
private:
	static std::unique_ptr<const Configuration> _INSTANCE;

	Configuration() = default;
	Configuration(const Configuration &copy) = delete;
	Configuration(Configuration &&move) = delete;
	Configuration &operator=(const Configuration &copy) = delete;
	Configuration &operator=(Configuration &&move) = delete;
public:
	const unsigned int     refresh_rate;
	const double           temperature;
	const std::vector<Conf_Fan> fans;

	Configuration(conf_parsed &&conf);

	static void reload(std::string &file);
	friend const Configuration &conf();
};

const Configuration &conf();

}