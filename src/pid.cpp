#include "pid.hpp"

PID::PID(double Kp, double Kd, double Ki, double sp) :
	_Kp(Kp),
	_Kd(Kd),
	_Ki(Ki),
	_sp(sp),
	_last_ts(0.0),
	_pre_error(0.0),
	_integral(0.0)
{}

const double
PID::evaluate(double ts, double pv)
{
	const double dt = (ts - _last_ts);
	const double error = _sp - pv;
	
	// Proportional term
	const double Pout = _Kp * error;
	// Integral term
	_integral += error * dt;
	const double Iout = _Ki * _integral;
	// Derivative term
	const double derivative = (error - _pre_error) / dt;
	const double Dout = _Kd * derivative;
	// Save state
	_pre_error = error;
	_last_ts = ts;

	return Pout + Iout + Dout;
}