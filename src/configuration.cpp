#include "configuration.hpp"

#include <iostream>
#include <utility>
#include <map>

#include <assert.h>

#include <yaml-cpp/yaml.h>

namespace rf {

std::unique_ptr<const Configuration> Configuration::_INSTANCE;

static std::vector<Conf_Fan>
factory_vector_Fan(conf_parsed &conf) {
	std::vector<Conf_Fan> out;
	std::map<std::string, std::shared_ptr<Conf_Policy>> policies;
	for (auto &policy : conf._policies) {
		switch (policy._type) {
		case conf_policy_type::TIMEOUT:
			{
				auto [_, inserted] = policies.insert_or_assign(
					policy._id,
					std::make_shared<Conf_Policy_Timeout>(std::get<unsigned int>(policy._value))
				);
				if (!inserted) {
					std::cerr << "Warning: Policy identificator ('id') colision!" << std::endl;
				}
			}
			break;
		case conf_policy_type::ACTIVE_THRESHOLD:
			{
				if (std::get<bool>(policy._value)) {
					auto [_, inserted] = policies.insert_or_assign(
						policy._id,
						std::make_shared<Conf_Policy_ActiveThreshold>(std::get<bool>(policy._value))
					);
					if (!inserted) {
						std::cerr << "Warning: Policy identificator ('id') colision!" << std::endl;
					}
				}
			}
			break;
		}
	}
	for (auto &_fan : conf._fans) {
		std::vector<std::shared_ptr<Conf_Policy>> fan_policies;
		std::transform(_fan._policies.begin(), _fan._policies.end(), std::back_inserter(fan_policies),
			[&](std::string &name) -> std::shared_ptr<Conf_Policy> {
				auto it = policies.find(name);
				if (it != policies.end()) {
					return it->second;
				} else {
					std::cerr << "Error: Policy '" << name << "' is missing!" << std::endl;
				}
			});
		auto f = out.emplace_back(_fan._offset, _fan._threshold, fan_policies);
	}
	return out;
}


Conf_Fan::Conf_Fan(const unsigned int offset, const unsigned int threshold, std::vector<std::shared_ptr<Conf_Policy>> &policies) :
	offset(offset),
	threshold(threshold),
	policies(policies)
{}

Conf_Policy_ActiveThreshold::Conf_Policy_ActiveThreshold(const bool enabled) : 
	enabled(enabled)
{}

Conf_Policy_Timeout::Conf_Policy_Timeout(const unsigned int timeout) :
	timeout(timeout)
{}

Configuration::Configuration(conf_parsed &&conf) :
	refresh_rate(conf._global._refresh_rate),
	temperature(conf._global._temperature),
	fans(factory_vector_Fan(conf))
{}

void
Configuration::reload(std::string &file)
{
	YAML::Node config = YAML::LoadFile(file);
	_INSTANCE = std::make_unique<const Configuration>(
			std::move(config.as<rf::conf_parsed>())
		);
}

const Configuration &
conf() {
	assert(Configuration::_INSTANCE.get() != NULL);

	return *(Configuration::_INSTANCE.get());
}

}