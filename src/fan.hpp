#pragma once

#include <vector>
#include <memory>

#include "configuration.hpp"

namespace rf {

class Policy {
public:
	virtual ~Policy();
};

class Policy_Active_Threshold final : public Policy {
public:
	virtual ~Policy_Active_Threshold();
};

class Policy_Timeout final : public Policy {
public:
	Policy_Timeout() = default;
	virtual ~Policy_Timeout();
};

class Fan final {
private:
	const unsigned int _threshold;
	const unsigned int _offset;
	std::vector<std::unique_ptr<Policy>> _policies;
public:
	Fan(const Conf_Fan &conf);
};

}