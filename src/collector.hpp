#pragma once

#include <tuple>
#include <chrono>

namespace rf {

typedef std::chrono::time_point<std::chrono::system_clock, std::chrono::duration<double, std::nano>> timestamp_t;

std::tuple<timestamp_t,	double> measure_temp();

}