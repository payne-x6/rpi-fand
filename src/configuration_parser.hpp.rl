#pragma once

#include <iostream>
#include <string>
#include <variant>
#include <vector>
#include <cstring>
#include <cstdlib>

#include <yaml-cpp/yaml.h>

namespace rf {

struct conf_pid {
	double _p = 0.0;
	double _d = 0.0;
	double _i = 0.0;	
};

struct conf_time_interval {
	int _interval;
	operator unsigned int();
};

struct conf_temperature {
	double _temp;
	operator double();
};

struct conf_global {
	struct conf_pid _pid;
	int _refresh_rate;
	double _temperature;
};

enum conf_policy_type {
	ACTIVE_THRESHOLD,
	TIMEOUT
};

typedef std::variant<bool, unsigned int> conf_policy_value_t;

struct conf_policy {
	std::string _id;
	conf_policy_type _type;
	conf_policy_value_t _value;
};

struct conf_fan {
	int _gpio;
	int _threshold;
	int _offset;
	std::vector<std::string> _policies;
};

struct conf_parsed {
	struct conf_global _global;
	std::vector<struct conf_policy> _policies;
	std::vector<struct conf_fan> _fans;
};
}

namespace YAML {

template<>
struct convert<rf::conf_pid> {
	static bool
	decode(const Node& node, rf::conf_pid& rhs) {
		if (!node.IsSequence() || node.size() == 0 || node.size() > 3) {
			return false;
		}
		switch (node.size()) {
			case 3:
				rhs._i = node[2].as<double>();
				[[fallthrough]];
			case 2:
				rhs._d = node[1].as<double>();
				[[fallthrough]];
			case 1:
				rhs._p = node[0].as<double>();
				return true;
			default:
				return false;
		}
	}
};

%%{
	machine time_interval;

	integer = ([1-9][0-9]{0,3});

	main :=
		integer@{ rhs._interval = rhs._interval * 10 + (fc - '0'); }
		('s'
			|'m'%{ rhs._interval *= 60; }
			|'h'%{ rhs._interval *= 3600; }
		)?;
}%%

template<>
struct convert<rf::conf_time_interval> {
	%% write data;

	static bool
	decode(const Node& node, rf::conf_time_interval& rhs) {
		if (node.IsSequence()) {
			return false;
		}

		std::string val = node.as<std::string>();
		rhs._interval = 0;
		const char *p = val.c_str();
		const char *pe = p + val.length();
		const char *eof = pe;
		int cs, act;

		%% write init;
		%% write exec;
		
		if (p != eof) {
			return false;
		}

		return true;
	}
};

%%{
	machine temperature;

	action store_char {
		*str_p = fc;
		str_p++;
	}

	action str_to_float {
		rhs._temp = std::atof(str);
	}

	float = [+\-]?@store_char[0-9]{1,3}@store_char('.'@store_char[0-9]{0,4}@store_char)?;
	
	main :=
		float%str_to_float('°'?
		('C'|'F'%{
			rhs._temp = (rhs._temp - 32.0) * (5.0/9.0);
		}
		))?;
}%%

template<>
struct convert<rf::conf_temperature> {
	%% write data;
	
	static bool
	decode(const Node& node, rf::conf_temperature& rhs)
	{
		if (node.IsSequence()) {
			return true;
		}

		std::string val = node.as<std::string>();
		char str[10] = "";
		char *str_p = str;
		const char *p = val.c_str();
		const char *pe = p + val.length();
		const char *eof = pe;
		int cs, act;

		%% write init;
		%% write exec;

		if (p != eof) {
			return false;
		}

		return true;
	}
};

template <>
struct convert<rf::conf_global> {
	static bool
	decode(const Node& node, rf::conf_global& rhs)
	{
		if (node.IsSequence()) {
			return false;
		}

		rhs._pid = node["pid"].as<rf::conf_pid>();
		rhs._refresh_rate = node["refresh-rate"].as<rf::conf_time_interval>();
		rhs._temperature = node["desired-temperature"].as<rf::conf_temperature>();
		
		return true;
	}
};

template <>
struct convert<rf::conf_policy> {
	static bool
	decode(const Node& node, rf::conf_policy& rhs)
	{
		if (node.IsSequence()) {
			return false;
		}

		rhs._id = node["id"].as<std::string>();
		std::string type = node["type"].as<std::string>();
		if(!type.compare("active-threshold")) {
			rhs._type = rf::conf_policy_type::ACTIVE_THRESHOLD;
			rhs._value = node["value"].as<bool>();
		} else if (!type.compare("timeout")) {
			rhs._type = rf::conf_policy_type::TIMEOUT;
			rhs._value = node["value"].as<rf::conf_time_interval>();
		} else {
			return false;
		}
		return true;
	}
};

template<>
struct convert<rf::conf_fan> {
	static bool
	decode(const Node& node, rf::conf_fan& rhs)
	{
		if (node.IsSequence()) {
			return false;
		}

		rhs._gpio = node["gpio"].as<int>();
		rhs._offset = node["offset"] ? node["offset"].as<int>() : 0;
		if (node["policies"]) {
			std::transform(node["policies"].begin(), node["policies"].end(),
		               std::back_inserter(rhs._policies),
		               [](const YAML::Node &node) -> std::string {
		                       return node.as<std::string>();
		               }
			);
		}
		rhs._threshold = node["threshold"] ? node["threshold"].as<int>() : 0;
		return true;
	}
};

template<>
struct convert<rf::conf_parsed> {
	static bool
	decode(const Node& node, rf::conf_parsed& rhs)
	{
		rhs._global = node["global"].as<rf::conf_global>();
		if (node["policies"]) {
			std::transform(node["policies"].begin(), node["policies"].end(),
			               std::back_inserter(rhs._policies),
			               [](const YAML::Node &node) -> rf::conf_policy {
			                       return node.as<rf::conf_policy>();
			               }
			);
		}
		std::transform(node["fans"].begin(), node["fans"].end(),
		               std::back_inserter(rhs._fans),
		               [](const YAML::Node &node) -> rf::conf_fan {
		                       return node.as<rf::conf_fan>();
		               }
		);
		return true;
	}
};
}