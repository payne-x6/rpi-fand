#pragma once

#include <exception>
#include <string>

namespace rf {

class Exception : public std::exception
{
private:
	const std::string _message;
	const int _errno;
public:
	Exception(const char *message, const int _errno = 0) :
		_message(message),
		_errno(_errno)
	{}

	const char *what() const throw ()
	{
		return _message.c_str();
	}

	const int get_errno() const {
		return _errno;
	}
};

}