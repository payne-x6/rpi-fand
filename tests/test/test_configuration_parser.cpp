#include "configuration_parser.hpp"

#include <gtest/gtest.h>

TEST(configuration_parser, time_interval) {
	YAML::Node time_interval;
	int ti_parsed;

	time_interval = YAML::Load("1");
	ti_parsed = time_interval.as<rf::conf_time_interval>();
	ASSERT_EQ(ti_parsed, 1) << "Parsed '1' as '" << ti_parsed << "' seconds";

	time_interval = YAML::Load("2s");
	ti_parsed = time_interval.as<rf::conf_time_interval>();
	ASSERT_EQ(ti_parsed, 2) << "Parsed '2s' as '" << ti_parsed << "' seconds";

	time_interval = YAML::Load("1m");
	ti_parsed = time_interval.as<rf::conf_time_interval>();
	ASSERT_EQ(ti_parsed, 60) << "Parsed '1m' as '" << ti_parsed << "' seconds";

	time_interval = YAML::Load("1h");
	ti_parsed = time_interval.as<rf::conf_time_interval>();
	ASSERT_EQ(ti_parsed, 3600) << "Parsed '1h' as '" << ti_parsed << "' seconds";
}

TEST(configuration_parser, temperature) {
	YAML::Node temperature;
	double temperature_parsed;

	temperature = YAML::Load("-100°C");
	temperature_parsed = temperature.as<rf::conf_temperature>();
	ASSERT_EQ(temperature_parsed, -100.0) << "Parsed '-100°C' as '" << temperature_parsed << "'";
	
	temperature = YAML::Load("0°C");
	temperature_parsed = temperature.as<rf::conf_temperature>();
	ASSERT_EQ(temperature_parsed, 0.0) << "Parsed '0°C' as '" << temperature_parsed << "'";

	temperature = YAML::Load("+100°C");
	temperature_parsed = temperature.as<rf::conf_temperature>();
	ASSERT_EQ(temperature_parsed, 100.0) << "Parsed '+100°C' as '" << temperature_parsed << "'";
}