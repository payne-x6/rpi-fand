macro (GENERATE_GRAPHS)
	find_program (DOT_EXECUTABLE NAMES dot DOC "path to the dot executable")
	if (NOT DOT_EXECUTABLE)
		message (FATAL_ERROR "Unable to find dot executable" )
	endif ()

	cmake_parse_arguments (vizualization "" "OUTPUT_DIRECTORY;OUTPUTS" "GRAPH" ${ARGN})

	list(LENGTH vizualization_GRAPH len)
	math(EXPR count "${len} - 1")

	file (MAKE_DIRECTORY ${vizualization_OUTPUT_DIRECTORY})
	foreach (it RANGE 0 ${count} 2)
		math(EXPR it2 "${it} + 1")
		list(GET vizualization_GRAPH ${it} file)
		list(GET vizualization_GRAPH ${it2} machine)

		RAGEL_TARGET (vizualization_${machine} ${file} ${vizualization_OUTPUT_DIRECTORY}/${machine}.dot COMPILE_FLAGS "-Vp -S ${machine}")
		add_custom_command (OUTPUT ${vizualization_OUTPUT_DIRECTORY}/${machine}.png
		                    COMMAND ${DOT_EXECUTABLE}
		                    ARGS ${vizualization_OUTPUT_DIRECTORY}/${machine}.dot -Tpng -o ${vizualization_OUTPUT_DIRECTORY}/${machine}.png
				    DEPENDS ${vizualization_OUTPUT_DIRECTORY}/${machine}.dot)
		list (APPEND ${vizualization_OUTPUTS} ${vizualization_OUTPUT_DIRECTORY}/${machine}.png)
	endforeach()

	unset (vizualization_GRAPH)
	unset (len)
	unset (count)
	unset (it)
	unset (it2)
endmacro ()